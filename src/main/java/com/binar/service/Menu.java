package com.binar.service;

    import java.util.Scanner;

    public class Menu implements IMainMenu{
        Scanner scan = new Scanner(System.in);
        String csvPath = "src/file/data_sekolah.csv";
        String saveMeanMedianModusPath = "src/file/Mean_Median_Modus_DataSekolah.txt";
        String saveModusSekolahPath = "src/file/Modus_Sekolah.txt";
        String saveKeduanyaPath = "src/file/File_gabungan.vsdtxt";

        @Override
        public void switchMenu(){
            MeanMedMod total = new MeanMedMod();

            switch (mainMenu()) {
                case 1:
                    total.write(saveMeanMedianModusPath);
                    break;
                case 2:
                    total.writeMod(saveModusSekolahPath);
                    break;
                case 3:
                    total.writeMix(saveKeduanyaPath);
                    break;
                case 0:
                    System.out.println("Anda telah keluar dari aplikasi...");
                    return;
                default:
                    this.mainMenu();
            }
        }
        @Override
        public int mainMenu() {
            System.out.println("==================================================");
            System.out.println("======= Aplikasi Pengolah Data Nilai Siswa =======");
            System.out.println("--------------------------------------------------");
            System.out.println("= 1. Generate File txt Mean-Median-Modus         =");
            System.out.println("= 2. Generate File txt Modus Sekolah             =");
            System.out.println("= 3. Generate File txt Kedua File                =");
            System.out.println("= 0. Exit                                        =");
            System.out.println("==================================================");
            System.out.print  ("= Masukkan Pilihan: ");
            return scan.nextInt();
        }
    }
